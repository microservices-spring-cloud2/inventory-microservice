package com.classpath.inventoryservice.consumer;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;
import com.classpath.inventoryservice.model.Order;
import org.springframework.cloud.stream.messaging.Sink;

@Service
public class OrdersConsumer {
	
    @StreamListener(Sink.INPUT)
    public void loggerSink(Order order) {
        System.out.println("===================================");
        System.out.println("Received order :"+ order);
        System.out.println("===================================");
    }
}
