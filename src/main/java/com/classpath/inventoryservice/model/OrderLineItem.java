package com.classpath.inventoryservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(of = "id")
public class OrderLineItem {

    @Setter @Getter
    private int id;

    @Setter @Getter
    private int qty;

    @Setter @Getter
    private double price;

    @JsonIgnore
    @Setter
    private Order order;
}